import json
import urllib
import logging
import math

from google.appengine.ext import ndb
import traceback

class User(ndb.Model):
    # authentification info
    email = ndb.StringProperty(default="")
    firstname = ndb.StringProperty(default="")
    lastname = ndb.StringProperty(default="")
    token = ndb.StringProperty(default="")
    avatar = ndb.StringProperty(default="")

    joinAt = ndb.DateTimeProperty(auto_now_add)
    position = ndb.GeoPtProperty(default=null)
    clazz = ndb.StringProperty(default="")
    promotion = ndb.IntegerProperty(default=0)
    description = ndb.TextProperty(default="")
    activated = ndb.BooleanProperty(default=false)
    tags = ndb.JsonProperty()


    save(user):
        user_key = user.put();
        return user_key;

    get_by_email(email):
        query = User.query(User.email == email);
        return query;
