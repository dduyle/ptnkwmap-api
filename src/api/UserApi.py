from src.api.BaseApi import *
from src.api.model import *
from firebase_idtoken import verify_token
from src.api.config.Config import *

class UserApi(BaseApi):
    def login(self):
        token = self.request.get('token', None)
        user = None
        try:
            result = verify_token(token, FIRE_BASE_PROJECT)
        except Exception as e:
            logging.error(e)
            result = None
        if result:
            uid = result['user_id']
            email = result['email']
            name = result['name']
            avatar = result['picture']
            user = User.get_by_email(email)
            if not user:
                # First time login
                if anonymous_user:
                    # User has anonymous data
                    user.create_from_anonymous(anonymous_user, uid, email, name, avatar)
                else:
                    # Create new user
                    user = User.create_user(uid, email, name, avatar)
            else:
                # Exists user
                if anonymous_user:
                    # Merge likes/dislikes data from anonymous user to user
                    logging.debug("start merge")
                    user.merge_from_anonymous_user(anonymous_user)
                # Update user info from FireBase token
                user.name = name
                user.avatar = avatar
                user.email = email
                if self.is_empty(user.token):
                    user.token = User.gen_token()
                user.update()
        if user is None:
            self.write_auth_error()
        else:
            self.encode_json_result(STATE_SUCCESS, 'Login success', user.to_json())
