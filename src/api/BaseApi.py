import json
import webapp2
from src.model.User import User
import logging
from datetime import datetime

LIMIT = "limit"
OFFSET = "offset"

STATE_SUCCESS = 200
STATE_UNAUTHORIZED = 401
STATE_NOT_FOUND = 404
STATE_INVALID_PARAMS = 400
STATE_PERMISSION_DENIED = 403
STATE_DUPLICATE = 409


class BaseApi(webapp2.RequestHandler):
    def verify_login(self, allow_anonymous=False):
        """
        :return: user id or None
        """
        token = self.request.get('token', None)
        uid = int(self.request.get('uid', -1))
        user = User.get_by_id(uid)
        if user and user.token == token:
            return user
        elif allow_anonymous:
            ip = self.request.remote_addr
            user = User.get_by_ip(ip)
            if not user:
                user = User.create_anonymous_user(ip)
            return user
        else:
            return None

    def verify_admin(self):
        user = self.verify_login()
        if not user:
            self.write_auth_error()
            return False
        elif not user.is_admin():
            self.encode_json_result(STATE_PERMISSION_DENIED, "Admin only")
            return False
        return True

    def get_offset_limit(self, default_offset=0, default_limit=30):
        offset = int(self.request.get('offset', default_offset))
        limit = int(self.request.get('limit', default_limit))
        return offset, limit

    @classmethod
    def json_serial(cls, obj):
        """JSON serializer for objects not serializable by default json code"""

        if isinstance(obj, datetime):
            serial = obj.isoformat()
            return serial
        raise TypeError("Type not serializable")

    def encode_json_result(self, state, msg='', extra={}):
        res = {
            'state': state,
            'message': msg,
            'data': extra
        }
        self.response.headers['Content-Type'] = 'application/json'
        self.response.headers['Access-Control-Allow-Origin'] = '*'
        self.response.headers['Access-Control-Allow-Methods'] = 'DELETE, HEAD, GET, OPTIONS, POST, PUT'
        self.response.headers['Access-Control-Allow-Headers'] = '*'
        logging.debug(extra)
        self.response.out.write(json.dumps(res, default=self.json_serial))

    def write_auth_error(self):
        self.encode_json_result(STATE_UNAUTHORIZED, 'UnAuthorized user')

    @webapp2.cached_property
    def session(self):
        # Returns a session using the default cookie key.
        return self.session_store.get_session()

    @classmethod
    def is_empty(cls, val):
        return val is None or len(val.strip()) == 0

    def options(self, *args, **kwargs):
        self.encode_json_result(STATE_SUCCESS, 'Done', {'message': 'Allow'})
