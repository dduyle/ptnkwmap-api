FIRE_BASE_CERTIFICATE ="-----BEGIN CERTIFICATE-----\nMIIDHDCCAgSgAwIBAgIIFj1HUxekwmYwDQYJKoZIhvcNAQEFBQAwMTEvMC0GA1UE" \
                       "\nAxMmc2VjdXJldG9rZW4uc3lzdGVtLmdzZXJ2aWNlYWNjb3VudC5jb20wHhcNMTYx" \
                       "\nMTE0MDA0NTI2WhcNMTYxMTE3MDExNTI2WjAxMS8wLQYDVQQDEyZzZWN1cmV0b2tl" \
                       "\nbi5zeXN0ZW0uZ3NlcnZpY2VhY2NvdW50LmNvbTCCASIwDQYJKoZIhvcNAQEBBQAD" \
                       "\nggEPADCCAQoCggEBAM6uECb3PYbaESHEWg9lr3e8PJ0G/vUtsglcAAZAyfFlJXMh" \
                       "\no9vfocZrltjlvuAVZepNSGyEWRvQEU6Y6Gs4fPVqPMLXidZAumNjRrGLzToud4q9" \
                       "\nRb2hx76K7/ougp1IauULiMtO7+/tjsSEL/uxdiMlNYxpT/gHL5uEOrSwKs+qkZ3E" \
                       "nzu96ZottgbwmFSS7Ljvu/dhSQW1spy7SK3QWru3ekJWTXwdcZO3y18WWrhVdBY07" \
                       "\nHG49590SVE3N2W3/EV4AauB1y9o8UxNxAyIPxhic0RxwPO3SIEQEssjfwdx4LbtJ" \
                       "\nR2hsj3PkWLlN9LoXX/Ddg4nW68sRr0HpwH+D1dsCAwEAAaM4MDYwDAYDVR0TAQH/" \
                       "\nBAIwADAOBgNVHQ8BAf8EBAMCB4AwFgYDVR0lAQH/BAwwCgYIKwYBBQUHAwIwDQYJ" \
                       "\nKoZIhvcNAQEFBQADggEBAKtLPf42iLtMGIbv5HorhAiNa9P2+K9OxMyKiMliCp58" \
                       "\nFkwxanrnQEEo0xJ2VAu8fOZFTgLfgNziYSSpOFNsiK2UIrCpFDFVJ4+R7E4dTchL" \
                       "\n92kz0lBArUjJZSNxMM4a0uhCsCqUP7IyGemOQxbxbANjY9X2CdA5ngWwJh9zzfaQ" \
                       "\nyTPsFEmIQTdVdt4f/ApfmWkUiTYAQqzZRNk7KS4QuY/ooH+/5g6sMva9VwXDESZY" \
                       "\njxt5pAfV23EKmb5mRUTXoLXB8bwXS8JekhEE0c0LSO0i3MW2CtkxZhatR5K1tR1w" \
                       "\nPMhMkz1pCIso1NPW1YGIWljqRowW+XusApFti98gyYA=\n-----END CERTIFICATE-----\n"

FIRE_BASE_PROJECT = "ptnkwmap-api"
