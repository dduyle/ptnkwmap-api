from __future__ import unicode_literals

from webapp2 import Route
import webapp2

from src.api.UserApi import UserApi

class PageNotFound(webapp2.RequestHandler):
    def get(self):
        self.error(404)

    def post(self):
        self.error(404)


#  ================================================
app = webapp2.WSGIApplication([
    # USER API
    Route('/api/v1/user/login', handler=UserApi, methods=['OPTIONS','POST'], handler_method='login')
])
